package com.codingchallenge;

import com.codingchallenge.utility.DataParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableAsync
public class Application {

	@Autowired
	private DataParser dataParser;
	private static String[] inputFileNames;

	@PostConstruct
	public void init() {
		if (inputFileNames != null) {
			dataParser.parseDataFromFiles(inputFileNames);
		}
	}

	public static void main(String[] args) {
		inputFileNames = args;
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(Application.class);
		configurableApplicationContext.close();
	}

}
