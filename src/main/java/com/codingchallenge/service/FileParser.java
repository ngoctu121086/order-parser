package com.codingchallenge.service;

public interface FileParser {
    void parseDataFromFiles(String fileName, String filePath);
}
