package com.codingchallenge.service;

import com.codingchallenge.common.Constant;
import com.codingchallenge.domain.Output;
import com.codingchallenge.utility.Utils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;


@Service
public class CsvFileParserImpl implements FileParser {
    @Async
    public void parseDataFromFiles(String fileName, String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new URL(filePath + "/" + fileName).openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            int rowNo = 1;
            while ((line = br.readLine()) != null) {
                if (rowNo > 1) { //ignore CSV header row
                    String[] tempArr = line.split(",");
                    if (tempArr.length >= 4) {
                        Output output = Output.builder()
                                .withId(tempArr[Constant.idColumnIndex])
                                .withAmount(tempArr[Constant.amountColumnIndex])
                                .withComment(tempArr[Constant.commentColumnIndex])
                                .withFilename(fileName)
                                .withLine(rowNo-1)
                                .collectResult();
                        System.out.println(Utils.toJson(output));
                    }
                }
                rowNo++;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
