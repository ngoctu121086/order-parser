package com.codingchallenge.service;

import com.codingchallenge.domain.Input;
import com.codingchallenge.domain.Output;
import com.codingchallenge.utility.Utils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


@Service
public class JsonFileParserImpl implements FileParser {
    @Async
    public void parseDataFromFiles(String fileName, String filePath) {
        InputStream inputStream = null;
        try {
            inputStream = new URL(filePath + "/" + fileName).openStream();
            List<Input> jsonInputList = Utils.parseJsonFileAsInputList(inputStream);
            int rowNo = 1;
            for(Input input: jsonInputList) {
                Output out = Output.builder()
                        .withId(input.getId())
                        .withAmount(input.getAmount())
                        .withComment(input.getComment())
                        .withFilename(fileName)
                        .withLine(rowNo++)
                        .collectResult();
                System.out.println(Utils.toJson(out));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
