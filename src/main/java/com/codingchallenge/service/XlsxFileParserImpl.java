package com.codingchallenge.service;

import com.codingchallenge.domain.Output;
import com.codingchallenge.utility.Utils;
import com.codingchallenge.common.Constant;
import org.apache.poi.ss.usermodel.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

@Service
public class XlsxFileParserImpl implements FileParser {
    private DataFormatter dataFormatter = new DataFormatter();

    @Async
    public void parseDataFromFiles(String fileName, String filePath) {
        InputStream inputStream = null;
        Workbook workbook = null;
        try {
            inputStream = new URL(filePath + "/" + fileName).openStream();
            workbook = WorkbookFactory.create(inputStream);
            Sheet mainSheet = workbook.cloneSheet(0);
            int rowNo = 0;
            Iterator<Row> mainSheetRowIterator = mainSheet.rowIterator();
            while (mainSheetRowIterator.hasNext()) {
                Row row = mainSheetRowIterator.next();
                if (rowNo > 0) { //ignore header row
                    Output output = Output.builder()
                                            .withFilename(fileName)
                                            .withLine(rowNo);
                    parseCell(output, row, Constant.idColumnIndex);
                    parseCell(output, row, Constant.amountColumnIndex);
                    parseCell(output, row, Constant.commentColumnIndex);
                    output.collectResult();

                    System.out.println(Utils.toJson(output));
                }
                rowNo++;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                workbook.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseCell(Output output, Row currentRow, int columnIndex) {
        String cellValue = dataFormatter.formatCellValue(currentRow.getCell(columnIndex));
        if (columnIndex == Constant.idColumnIndex) {
            output.withId(cellValue);
        } else if (columnIndex == Constant.amountColumnIndex) {
            output.withAmount(cellValue);
        } else if (columnIndex == Constant.commentColumnIndex) {
            output.withComment(cellValue);
        }
    }
}
