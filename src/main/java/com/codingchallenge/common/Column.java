package com.codingchallenge.common;

public enum Column {
    ID("id"),
    AMOUNT("amount"),
    CURRENCY("currency"),
    COMMENT("comment");
    private String name;

    Column(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
