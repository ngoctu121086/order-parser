package com.codingchallenge.common;

public class Constant {
    public static int idColumnIndex = 0;
    public static int amountColumnIndex = 1;
    public static int currencyColumnIndex = 2;
    public static int commentColumnIndex = 3;

    public static String ok = "OK";
    public static String NUMERIC_PARSING_ERROR_MESSAGE = "Invalid numeric value. Please check field/column '%s' = '%s'";
}
