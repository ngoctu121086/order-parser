package com.codingchallenge.utility;

import com.codingchallenge.Application;
import com.codingchallenge.service.FileParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Optional;


@Service
public class DataParser {
    @Autowired
    @Qualifier("csvFileParserImpl")
    private FileParser csvFileParser;
    @Autowired
    @Qualifier("jsonFileParserImpl")
    private FileParser jsonFileParser;
    @Autowired
    @Qualifier("xlsxFileParserImpl")
    private FileParser xlsxFileParser;

    //file path will be on the same folder with application JAR file
    private String filePath = new File(Application.class.getClassLoader().getResource("").getPath()).getParentFile().getParentFile().getParent();

    public void parseDataFromFiles(String[] fileNames) {
        for (String fileName : fileNames) {
            String fileExtension = Utils.getFileExtension(fileName);
            Optional.of(fileExtension).ifPresent(s -> {
                switch (s.toUpperCase()) {
                    case "CSV":
                        csvFileParser.parseDataFromFiles(fileName, filePath);
                        break;
                    case "JSON":
                        jsonFileParser.parseDataFromFiles(fileName, filePath);
                        break;
                    case "XLSX":
                        xlsxFileParser.parseDataFromFiles(fileName, filePath);
                        break;
                }
            });
        }
    }
}
