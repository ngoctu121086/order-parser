package com.codingchallenge.utility;

import com.codingchallenge.domain.Input;
import com.codingchallenge.domain.Output;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Map;


public class Utils {
    private static ObjectMapper objectMapper = new ObjectMapper();
    public static String toJson(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
    public static Map<String, String> parseJsonAsMap(String body) {
        try {
            return objectMapper.readValue(body, new TypeReference<Map<String, String>>() {});
        } catch (IOException e) {
            throw new UncheckedIOException("Invalid json file: " + body, e);
        }
    }
    public static List<Map<String, String>> parseJsonFileAsMap(InputStream jsonFileStream) {
        try {
            return objectMapper.readValue(jsonFileStream, new TypeReference<List<Map<String, String>>>() {});
        } catch (IOException e) {
            throw new UncheckedIOException("Invalid json file: " , e);
        }
    }
    public static List<Input> parseJsonFileAsInputList(InputStream jsonFileStream) {
        try {
            return objectMapper.readValue(jsonFileStream, new TypeReference<List<Input>>() {});
        } catch (IOException e) {
            throw new UncheckedIOException("Invalid json file: " , e);
        }
    }
    public static List<Output> parseJsonFileAsOutputList(InputStream jsonFileStream) {
        try {
            return objectMapper.readValue(jsonFileStream, new TypeReference<List<Output>>() {});
        } catch (IOException e) {
            throw new UncheckedIOException("Invalid json file: " , e);
        }
    }
    public static String getFileExtension(String rawfileName) {
        if (rawfileName != null) {
            String[] tempArr = rawfileName.split("\\.");
            return tempArr[tempArr.length - 1];
        }
        return "";
    }
}
