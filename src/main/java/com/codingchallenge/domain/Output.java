package com.codingchallenge.domain;

import com.codingchallenge.common.Column;
import com.codingchallenge.common.Constant;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Output {
    private Integer id;
    private Double amount;
    private String comment;
    private String filename;
    private Integer line;
    private String result;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Output withId(String id) {
        try {
            Double d = Double.parseDouble(id);
            if (d == d.intValue()) {
                this.id = d.intValue();
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            if (result == null) {
                this.setResult(String.format(Constant.NUMERIC_PARSING_ERROR_MESSAGE, Column.ID.getName(), id));
            } else {
                this.setResult(result + "|" + String.format(Constant.NUMERIC_PARSING_ERROR_MESSAGE, Column.ID.getName(), id));
            }
        }
        return this;
    }

    public Output withAmount(String amount) {
        try {
            this.amount = Double.parseDouble(amount);
        } catch (NumberFormatException e) {
            if (result == null) {
                this.setResult(String.format(Constant.NUMERIC_PARSING_ERROR_MESSAGE, Column.AMOUNT.getName(), amount));
            } else {
                this.setResult(result + "|" + String.format(Constant.NUMERIC_PARSING_ERROR_MESSAGE, Column.AMOUNT.getName(), amount));
            }
        }
        return this;
    }

    public Output withComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Output withFilename(String filename) {
        this.filename = filename;
        return this;
    }

    public Output withLine(Integer line) {
        this.line = line;
        return this;
    }

    public Output collectResult() {
        if (result == null)
            setResult(Constant.ok);
        return this;
    }

    public static Output builder() {
        return new Output();
    }

    // Custom filter that will omit null and "custom_string" values.
    public class IntegerFilter {
        @Override
        public boolean equals(Object other) {
            if (other == null) {
                // Filter null's.
                return false;
            }

            // Filter "custom_string".
            return "custom_string".equals(other);
        }
    }
}
